## Rails 7 Ballast

This project is a skeleton Ruby on Rails application that serves as a common foundation with opinionated best-practices defaults for you to build your application on top of.

For more information, see [doc/BALLAST.md](doc/BALLAST.md).
