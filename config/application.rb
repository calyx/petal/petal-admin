require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module RailsBallast
  class Application < Rails::Application

    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    # https://guides.rubyonrails.org/configuring.html

    config.load_defaults 7.0
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
