#
# reset passwords using a recovery code
#

class AuthenticatedApp::ResetWithCodeController < AuthenticatedApp::ResetController

  # get /reset_with_code/enter_code
  def enter_code
  end

  # get /reset_with_code/enter_password
  def enter_password
  end

  # put /reset_with_code/check_code
  def check_code
    if !username_and_code_present?
      render :enter_code
      return
    end
    @user, @recovery_code = get_user_and_code
    if @user
      session[:recovery_user] = @user.login
      session[:recovery_code] = @recovery_code
      redirect_to reset_with_code_enter_password_url
    else
      render :enter_code
    end
  end

  # put /reset_with_code/update_password
  def update_password
    if !username_and_code_present?
      render :enter_code
      return
    end
    @user, @recovery_code = get_user_and_code
    if !@user
      render :enter_code
    elsif !password_ok?(@user)
      render :enter_password
    elsif !save_new_password(@user)
      render :enter_password
    else
      login_as @user
    end
  end

  protected

  def username_and_code_present?
    if !params[:recovery_user].present?
      @errors[:recovery_user] = t(:username_is_required)
    end
    if !params[:recovery_code].present?
      @errors[:recovery_code] = t(:recovery_is_required)
    end
    return @errors.empty?
  end

  def save_new_password(user)
    user.reset_password!(
      recovery_code: params[:recovery_code],
      new_password: params[:new_password]
    )
  rescue StandardError => exc
    @errors = @user.errors
    flash.now[:danger] << t(:changes_not_saved)
  end

  #
  # authenticate with a recovery code, cleaning up username and recovery
  # code so as to reduce user errors.
  #
  # * trim whitespace
  # * downcase codes
  # * add hypens if the user left them out.
  #
  # params is modified directly, because these values are later used
  # to build the form which gets submitted in the next stage
  #
  def get_user_and_code
    user, recovery_code = nil
    login = (params[:recovery_user]||"").strip.downcase
    recovery_code = (params[:recovery_code]||"").strip.downcase.gsub('-','')
    user = User.authenticate_service(login, recovery_code, :recovery)
    unless user
      recovery_code = recovery_code.scan(/.{#{Conf.recovery_code_split||4}}/).join('-')
      user = User.authenticate_service(login, recovery_code, :recovery)
      unless user
        @errors[:recovery_user] = true
        @errors[:recovery_code] = true
        flash.now[:danger] << t(:username_or_recovery_code_is_incorrect)
      end
    end
    may_authenticate!(user)
    return user, recovery_code
  end

end
