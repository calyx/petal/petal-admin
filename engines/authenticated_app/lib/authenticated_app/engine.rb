module AuthenticatedApp
  class Engine < ::Rails::Engine
    isolate_namespace AuthenticatedApp
  end
end
