#
# Generic gemspec for local gems
#
Gem::Specification.new do |spec|
  spec.name        = File.basename(File.expand_path('..', __FILE__))
  spec.version     = ""
  spec.summary     = ""
  spec.authors     = ""
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*"]
  end
end
