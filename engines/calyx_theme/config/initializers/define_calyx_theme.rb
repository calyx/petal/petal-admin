Theme.config.apply_defaults({
  favicon: "/assets/favicon.png",
  header_bg: "#7CAB2B",
  header_icon_bg: "#6d9326",
  header_icon_url: "/assets/calyx-icon.svg",
  header_icon_position: "bottom center",
  header_image_url: "/assets/admin-header.svg",
  header_text: nil
})