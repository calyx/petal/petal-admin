require_relative 'theme/configuration'
require_relative 'theme/engine'

module Theme
  def self.configure
    yield config
  end

  def self.config
    @config ||= Theme::Configuration.new
  end
end
