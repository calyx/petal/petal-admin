require_relative 'task_helper'

class SubtreeEngine < TaskHelper
  attr_reader :name

  class << self
    def config_path
      "engines/*.yml"
    end
  
    def config_default
      "default.yml"
    end
  
    def config
      @config ||= begin
        config = {}
        paths = Dir.glob(File.join(Rails.root, config_path)).sort {|a,b|
          File.basename(a) == config_default ? -1 : a <=> b
        }
        paths.each do |file_path|
          begin
            file_hash = YAML.safe_load(File.read(file_path))
            if file_hash.is_a?(Hash)
              config.merge!(file_hash)
            end
          rescue StandardError => exc
            puts "FAILED to parse #{file_path}"
            puts exc.to_s
            exit
          end
        end
        config
      end
    end
  
    def each_engine(&block)
      config.each do |name, repo_config|
        yield SubtreeEngine.new(name, repo_config)
      end
    end
  end

  def initialize(name, options)
    @name = name
    @options = options
    @errors = []
  end

  def full_path
    File.join(Rails.root, @options["local_path"])
  end

  def local_path
    @options["local_path"]
  end

  def branch
    @options["branch"]
  end

  def pull
    @options["pull"]
  end

  def push
    @options["push"]
  end

  # true if a real subtree exists
  def exist?
    File.directory?(full_path)
  end

  # true if file is completely missing and not just a symlink
  # (don't return true if symlink exists but points to a missing file)
  def missing?
    !(File.file?(full_path) || File.directory?(full_path) || File.symlink?(full_path))
  end

  # does the subtree configuration contain the required options?
  def valid?
    %w[local_path branch pull].each do |key|
      if @options[key].nil? || @options[key] == ""
        @errors << "entry `#{name}` is missing a value for `#{key}`"
        return false
      end
    end
    true
  end 

  def git_valid?
    true
  end

  def git_add
    stash do
      run("git", "subtree", "add", "--prefix", local_path, pull, branch, "--squash")
    end
    ok("added %s" % local_path)
  end

  def git_pull
    stash do
      run("git", "subtree", "pull", "--prefix", local_path, pull, branch, "--squash")
    end
    ok("%s up to date" % local_path)
  end 

  def git_push
    if push
      run("git", "subtree", "push", "--prefix", local_path, push, branch)
    end
  end
end

namespace :engines do
  desc "Pulls upstream changes for engines (if argument given, only pull that repo)"
  task :pull, [:repo] do |task, args|
    SubtreeEngine.each_engine do |engine|
      next if args[:repo] && engine.name != args[:repo]
      if !engine.valid?
        engine.print_errors
      else
        if engine.exist?
          if !engine.git_valid?
            engine.print_errors
          else
            engine.git_pull
          end
        elsif engine.missing?
          engine.git_add
        end
      end
    end
  end
  
  desc "Pushes engine changes (if argument is given, only push that repo)"
  task :push, [:repo] do |task, args|
    SubtreeEngine.each_engine do |engine|
      next if args[:repo] && engine.name != args[:repo]
      if !engine.valid?
        engine.print_errors
      else
        if engine.exist?
          if !engine.git_valid?
            engine.print_errors
          else
            engine.git_push
          end
        elsif engine.missing?
          engine.bad("%s does not exist. Run pull first" % engine.local_path)
        end
      end
    end
  end

  desc "List engines"
  task :ls do
    in_git_history = `git log | grep git-subtree-dir | awk '{ print $2 }'`.strip.split("\n")
    SubtreeEngine.each_engine do |engine|
      if in_git_history.include?(engine.local_path)
        engine.ok(engine.local_path)
      else
        engine.bad("%s (not in git history)" % engine.local_path)
      end
    end
  end
end

